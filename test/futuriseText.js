const test = require('baretest')('Futurise text');
const assert = require('assert');
const futurus = require('../dist/main').default;

const testcases = [
    [
        'Umrzeć — tego nie robi się kotu.\r\nBo co ma począć kot\r\nw pustym mieszkaniu.',
        'Umżeć — tego ńe robi śę kotu.\r\nBo co ma począć kot\r\nw pustym mieszkańu.',
    ],
    [
        'pytanie „czy wolno jest użyć słowa, którego nie ma w słowniku” jest jak pytanie „czy wolno ugotować danie, którego nie ma w książce kucharskiej”',
        'pytańe „czy wolno jest użyć słowa, kturego ńe ma w słowńiku” jest jak pytańe „czy wolno ugotować dańe, kturego ńe ma w kśążce kuharskiej”',
    ],
];

for (let [input, expected] of testcases) {
    test(`${input} -> ${expected}`, async () => {
        assert.equal(futurus.futuriseText(input), expected);
    })
}

module.exports = test;

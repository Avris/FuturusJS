(async function() {
    await require('./futuriseWord.js').run();
    await require('./futuriseText.js').run();
})();

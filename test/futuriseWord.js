const test = require('baretest')('Futurise word');
const assert = require('assert');
const futurus = require('../dist/main').default;

const testcases = [
    ['ziele', 'źele'],
    ['wyrażenia', 'wyrażeńa'],
    ['różnych', 'rużnyh'],
    ['znaków', 'znakuw'],
    ['nie', 'ńe'],
    ['różnią', 'rużńą'],
    ['posiadając', 'pośadając'],
    ['sieć', 'śeć'],
    ['sposób', 'sposub'],
    ['wzór', 'wzur'],
    ['spółgłosek', 'spułgłosek'],
    ['jakich', 'jakih'],
    ['chwilą', 'hwilą'],
    ['przełamania', 'pszełamańa'],
    ['manifest', 'mańifest'],
    ['ortografii', 'ortografji'],
    ['pisowni', 'pisowńi'],
    ['niepotrzebne', 'ńepotszebne'],
    ['materiału', 'materjału'],
    ['się', 'śę'],
    ['nich', 'ńih'],
    ['przez', 'pszez'],
    ['rzeczywiście', 'żeczywiśće'],
    ['tych', 'tyh'],
    ['powstrzymujemy', 'powstszymujemy'],
    ['łyżka', 'łyżka'],
    ['trzy', 'tszy'],
    ['brzuch', 'bżuh'],
    ['buzi', 'buźi'],
    ['sito', 'śito'],
    ['cisza', 'ćisza'],
    ['zimno', 'źimno'],
    ['Chociaż', 'Hoćaż'],
    ['RZUT', 'ŻUT'],
    ['MANIFEST', 'MAŃIFEST'],
    ['rozpromienić', 'rozpromieńić'],
];

for (let [input, expected] of testcases) {
    test(`${input} -> ${expected}`, async () => {
        assert.equal(futurus.futuriseWord(input), expected);
    })
}

module.exports = test;

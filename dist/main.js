function $parcel$defineInteropFlag(a) {
  Object.defineProperty(a, '__esModule', {value: true, configurable: true});
}
function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}

$parcel$defineInteropFlag(module.exports);

$parcel$export(module.exports, "default", () => $349a00930b14e029$export$2e2bcd8739ae039);
const $349a00930b14e029$var$SOFTS = [
    [
        'z',
        'ź'
    ],
    [
        's',
        'ś'
    ],
    [
        'n',
        'ń'
    ],
    [
        'c',
        'ć'
    ]
];
const $349a00930b14e029$var$VOWELS = 'aoeiuyąę';
var $349a00930b14e029$export$2e2bcd8739ae039 = new class Futurus {
    futuriseWord(word) {
        return this._keepCase(word, this._futuriseWord);
    }
    _keepCase(word, callback) {
        const isFirstCapital = word.substring(0, 1).toLowerCase() !== word.substring(0, 1);
        const isRestCapital = word.substring(1).toLowerCase() !== word.substring(1);
        word = word.toLowerCase();
        word = callback(word);
        if (isFirstCapital) word = word.substring(0, 1).toUpperCase() + word.substring(1);
        if (isRestCapital) word = word.substring(0, 1) + word.substring(1).toUpperCase();
        return word;
    }
    _futuriseWord(word) {
        word = word.replace(/ch/g, 'h').replace(/ó/g, 'u');
        word = word.replace(/([tp])rz/g, '$1sz').replace(/([^tp])rz/g, '$1ż').replace(/^rz/g, 'ż');
        for (let [hard, soft] of $349a00930b14e029$var$SOFTS)word = word.replace(new RegExp(`${hard}i([${$349a00930b14e029$var$VOWELS}])`, 'g'), soft + '$1').replace(new RegExp(`${hard}i([^${$349a00930b14e029$var$VOWELS}])`, 'g'), soft + 'i$1').replace(new RegExp(`${hard}i$`, 'g'), soft + 'i');
        word = word.replace(new RegExp(`([fr])i([${$349a00930b14e029$var$VOWELS}])`, 'g'), '$1j$2');
        return word;
    }
    futuriseText(text) {
        return text.replace(/[a-ząćęłńóśźż]+/ig, (word)=>{
            return this.futuriseWord(word);
        });
    }
};


//# sourceMappingURL=main.js.map

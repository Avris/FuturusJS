const $e9036bc7e4b4ba73$var$SOFTS = [
    [
        'z',
        'ź'
    ],
    [
        's',
        'ś'
    ],
    [
        'n',
        'ń'
    ],
    [
        'c',
        'ć'
    ]
];
const $e9036bc7e4b4ba73$var$VOWELS = 'aoeiuyąę';
var $e9036bc7e4b4ba73$export$2e2bcd8739ae039 = new class Futurus {
    futuriseWord(word) {
        return this._keepCase(word, this._futuriseWord);
    }
    _keepCase(word, callback) {
        const isFirstCapital = word.substring(0, 1).toLowerCase() !== word.substring(0, 1);
        const isRestCapital = word.substring(1).toLowerCase() !== word.substring(1);
        word = word.toLowerCase();
        word = callback(word);
        if (isFirstCapital) word = word.substring(0, 1).toUpperCase() + word.substring(1);
        if (isRestCapital) word = word.substring(0, 1) + word.substring(1).toUpperCase();
        return word;
    }
    _futuriseWord(word) {
        word = word.replace(/ch/g, 'h').replace(/ó/g, 'u');
        word = word.replace(/([tp])rz/g, '$1sz').replace(/([^tp])rz/g, '$1ż').replace(/^rz/g, 'ż');
        for (let [hard, soft] of $e9036bc7e4b4ba73$var$SOFTS)word = word.replace(new RegExp(`${hard}i([${$e9036bc7e4b4ba73$var$VOWELS}])`, 'g'), soft + '$1').replace(new RegExp(`${hard}i([^${$e9036bc7e4b4ba73$var$VOWELS}])`, 'g'), soft + 'i$1').replace(new RegExp(`${hard}i$`, 'g'), soft + 'i');
        word = word.replace(new RegExp(`([fr])i([${$e9036bc7e4b4ba73$var$VOWELS}])`, 'g'), '$1j$2');
        return word;
    }
    futuriseText(text) {
        return text.replace(/[a-ząćęłńóśźż]+/ig, (word)=>{
            return this.futuriseWord(word);
        });
    }
};


export {$e9036bc7e4b4ba73$export$2e2bcd8739ae039 as default};
//# sourceMappingURL=module.js.map

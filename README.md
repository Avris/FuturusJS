# Avris Futurus

Narzędzie generuje na podstawie polskiego tekstu wejściowego tekst zapisany ortografią zgodną z Mańifestem Jasieńskiego.

Więcej informacji: [https://futurysci.avris.it](futurysci.avris.it)

    import futurus from 'avris-futurus';

    console.log(futurus.futuriseWord('wrzesień')); // wżeśeń
    
    console.log(futurus.futuriseText('Umrzeć - tego nie robi się kotu')); // Umżeć - tego ńe robi śę kotu
